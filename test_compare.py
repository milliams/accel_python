import numpy as np
from numba import jit, njit, prange

X = np.random.random((5000, 3))


def pairwise_numpy(X):
    return np.sqrt(((X[:, None, :] - X) ** 2).sum(-1))


def test_pairwise_numpy(benchmark):
    benchmark(pairwise_numpy, X.copy())


def pairwise_python(X):
    M = X.shape[0]
    N = X.shape[1]
    D = np.empty((M, M), dtype=np.float64)
    for i in range(M):
        for j in range(M):
            d = 0.0
            for k in range(N):
                tmp = X[i, k] - X[j, k]
                d += tmp * tmp
            D[i, j] = np.sqrt(d)
    return D


def test_pairwise_python(benchmark):
    benchmark(pairwise_python, X.copy())


pairwise_numba = jit(pairwise_python)


def test_pairwise_numba(benchmark):
    benchmark(pairwise_numba, X.copy())


pairwise_numba_nopython = jit(nopython=True)(pairwise_python)


def test_pairwise_numba_nopython(benchmark):
    benchmark(pairwise_numba_nopython, X.copy())


pairwise_numba_parallel = jit(nopython=True, parallel=True)(pairwise_python)


def test_pairwise_numba_parallel(benchmark):
    benchmark(pairwise_numba_parallel, X.copy())


@jit(parallel=True)
def pairwise_python_pjit(X):
    M = X.shape[0]
    N = X.shape[1]
    D = np.empty((M, M), dtype=np.float64)
    for i in prange(M):
        for j in prange(M):
            d = 0.0
            for k in prange(N):
                tmp = X[i, k] - X[j, k]
                d += tmp * tmp
            D[i, j] = np.sqrt(d)
    return D


def test_pairwise_python_pjit(benchmark):
    benchmark(pairwise_python_pjit, X.copy())


if __name__ == "__main__":
    import io
    import memory_profiler
    with io.StringIO() as stream:
        memory_profiler.profile(pairwise_numpy, stream=stream)(X.copy())
        print(stream.getvalue())
    #memory_profiler.profile(pairwise_python)(X.copy())
